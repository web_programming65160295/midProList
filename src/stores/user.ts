import { ref, nextTick } from 'vue';
import { defineStore } from 'pinia'
import type { User } from '@/types/User';

export const useUserStore = defineStore('user', () => {
  const dialog = ref(false);
  const dialogDelete = ref(false);
  

  const initiUser: User = {
    id: -1,
    fullName: "",
    email: "",
    tel: "",
    address: "",
    rank: "พนักงาน",
    status: "Active",
    password: "",
    user: "",
  };

  const editedUser = ref<User>(JSON.parse(JSON.stringify(initiUser)));
  let editedIndex = -1;
  const currentUser = ref<User|null>( 
    {
      id: 1,
      fullName: 'กฤติน ศรสุข',
      email: 'krit@example.com',
      tel: '0881234567',
      address: '169 Long Had Bangsaen Rd, Saen Suk, Chon Buri District, Chon Buri 20131',
      rank: 'พนักงานขาย',
      status: 'Active',
      password: 'pass1234',
      user: 'Krit'
    },
  )


  
    const users = ref<User[]>([
      {
        id: 1,
        fullName: 'กฤติน ศรสุข',
        email: 'krit@example.com',
        tel: '0881234567',
        address: '169 Long Had Bangsaen Rd, Saen Suk, Chon Buri District, Chon Buri 20131',
        rank: 'พนักงานขาย',
        status: 'Active',
        password: 'pass1234',
        user: 'Krit'
      },
      {
        id: 2,
        fullName: 'ปัณณทัต มั่งเลิศ',
        email: 'punapat@example.com',
        tel: '0891234567',
        address: '169 Long Had Bangsaen Rd, Saen Suk, Chon Buri District, Chon Buri 20131',
        rank: 'พนักงานขาย',
        status: 'Active',
        password: 'pass1234',
        user: 'Punna'
      },
      {
        id: 3,
        fullName: 'สุขอนันต์ กลับบ้านเก่า',
        email: 'sukja@example.com',
        tel: '0801234567',
        address: '169 Long Had Bangsaen Rd, Saen Suk, Chon Buri District, Chon Buri 20131',
        rank: 'พนักงานขาย',
        status: 'Active',
        password: 'pass1234',
        user: 'Sukja'
      }
    ])

    function closeDelete() {
      dialogDelete.value = false;
      nextTick(() => {
        editedUser.value = Object.assign({}, initiUser);
      });
    }
  
    function deleteItemConfirm() {
      users.value.splice(editedIndex, 1);
      closeDelete();
    }
  
    function editItem(item: User) {
      editedIndex = users.value.indexOf(item);
      editedUser.value = Object.assign({}, item);
      dialog.value = true;
    }
  
    function deleteItem(item: User) {
      editedIndex = users.value.indexOf(item);
      editedUser.value = Object.assign({}, item);
      dialogDelete.value = true;
    }

    function initialize() {
      users.value = [
        {
          id: 1,
          fullName: 'กฤติน ศรสุข',
          email: 'krit@example.com',
          tel: '0881234567',
          address: '169 Long Had Bangsaen Rd, Saen Suk, Chon Buri District, Chon Buri 20131',
          rank: 'พนักงานขาย',
          status: 'Active',
          password: 'pass1234',
          user: 'Krit'
        },
        {
          id: 2,
          fullName: 'ปัณณทัต มั่งเลิศ',
          email: 'punapat@example.com',
          tel: '0891234567',
          address: '169 Long Had Bangsaen Rd, Saen Suk, Chon Buri District, Chon Buri 20131',
          rank: 'พนักงานขาย',
          status: 'Active',
          password: 'pass1234',
          user: 'Punna'
        },
        {
          id: 3,
          fullName: 'สุขอนันต์ กลับบ้านเก่า',
          email: 'sukja@example.com',
          tel: '0801234567',
          address: '169 Long Had Bangsaen Rd, Saen Suk, Chon Buri District, Chon Buri 20131',
          rank: 'พนักงานขาย',
          status: 'Active',
          password: 'pass1234',
          user: 'Sukja'
        }
      ];
    
    }
    const author = (user: string, password: string) => {
      const index = users.value.findIndex((item) => item.user === user)
      if (index < 0) {
          currentUser.value = null
      }else{
        console.log(users.value[index].password)
        console.log(password)
        if(users.value[index].password === password){
          currentUser.value = users.value[index]
        }else{
          currentUser.value = null
        }
      }
      
  }

  function logout() {
    currentUser.value =(null)
  }
    

  return { users,dialog,dialogDelete,initiUser,editedUser,editedIndex,deleteItemConfirm,editItem,deleteItem,closeDelete,initialize,author,currentUser,logout }
})